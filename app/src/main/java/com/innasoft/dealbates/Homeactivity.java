package com.innasoft.dealbates;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Homeactivity extends AppCompatActivity {
    boolean doubleBackToExitPressedOnce = false;
    //String url ="https://www.dealbates.com/new-launches";
    String url ="https://www.dealbates.com/mobiles-offers/smart-phones-offers/realme-5i-4gb-ram-64gb-storage/?DB4056";
    @BindView(R.id.webview)
    WebView webview;
    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homeactivity);
        ButterKnife.bind(this);

        webview.setWebViewClient(new WebViewClient());
        webview.getSettings().setJavaScriptEnabled(true);
        webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webview.getSettings().setBuiltInZoomControls(false);
        webview.loadUrl(url);
        webview.setClickable(false);

        initControls();
    }


    private void initControls() {

        progressLayout.setVisibility(View.VISIBLE);
        webview.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //To open hyperlink in existing WebView
                view.loadUrl(url);
                return false;
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(getApplicationContext(), description, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                progressLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                progressLayout.setVisibility(View.GONE);
                /*view.loadUrl("javascript:(function() { " +
                        "var head = document.getElementsByClassName('header')[0].style.display='none'; " +
                        "})()");*/
                //view.loadUrl("javascript:var elements = document.getElementsByClassName('row pb-5'); footer.parentNode.removeChild(footer); var header = document.getElementsByClassName('affix-top'); header.parentNode.removeChild(header);");
                view.loadUrl("javascript:var elements = document.getElementsByClassName('affix-top'); console.log( elements[0].parentNode.removeChild(elements[0])); " +
                        "var elements2 = document.getElementsByClassName('bread-bg mb-5'); console.log( elements2[0].parentNode.removeChild(elements2[0])); " +
                        "var footer = document.getElementsByClassName('row pb-5'); console.log( footer[0].parentNode.removeChild(footer[0])); " +
                        "var footer2 = document.getElementsByClassName('browse-tags py-5'); console.log( footer2[0].parentNode.removeChild(footer2[0]));");
                super.onPageFinished(view,url);
            }
        });



    }
    @Override
    public void onBackPressed() {
        if (webview.canGoBack()) {
            webview.goBack();
        } else {
            super.onBackPressed();
        }
    }
}
